import os
os.chdir(os.path.dirname(__file__))

import bottle
from avavad import routes

application = bottle.default_app()
