from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from avavad.tables import *

engine = create_engine("sqlite:///db.sqlite3")
Session = sessionmaker(bind=engine)
db = Session()
