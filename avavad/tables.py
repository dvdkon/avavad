import os
import hashlib
import crypt
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from avavad import config

Base = declarative_base()

class Account(Base):
    __tablename__ = "accounts"

    id = Column(Integer, primary_key=True)
    # XXX: Lazy way of authorisation
    role = Column(String)

    unix_user = relationship("UnixUser", back_populates="account",
                             uselist=False)
    google_auth = relationship("GoogleAuth", back_populates="account",
                               uselist=False)
    password_auth = relationship("PasswordAuth", back_populates="account",
                                 uselist=False)

class UnixUser(Base):
    __tablename__ = "unix_users"

    uid = Column(Integer, primary_key=True)
    username = Column(String, unique=True)
    pwhash = Column(String)
    user_id = Column(Integer, ForeignKey(Account.id))

    account = relationship(Account, back_populates="unix_user", uselist=False)

    def set_password(self, pw):
        self.pwhash = crypt.crypt(pw, crypt.METHOD_SHA512)

class GoogleAuth(Base):
    __tablename__ = "google_auth"

    address = Column(String, primary_key=True)
    user_id = Column(Integer, ForeignKey(Account.id))


    account = relationship(
        Account, back_populates="google_auth", uselist=False)

class PasswordAuth(Base):
    __tablename__ = "password_auth"

    id = Column(Integer, primary_key=True)
    username = Column(String, unique=True)
    pwhash = Column(String)
    user_id = Column(Integer, ForeignKey(Account.id))

    account = relationship(
        Account, back_populates="password_auth", uselist=False)

    def set_password(self, pw):
        salt = os.urandom(32)
        pwhash = hashlib.scrypt(pw.encode(), salt=salt, **config.SCRYPT_PARAMS)
        pwhash_db = f"{pwhash.hex()}:{salt.hex()}"
        self.pwhash = pwhash_db
