import bottle
import bottle.ext.sqlalchemy as balch
from sqlalchemy import create_engine
from avavad import config, cors

db_engine = create_engine(config.DB_CONNSTR)

bottle.install(balch.Plugin(
    db_engine
))

cors.register_cors_plugin(bottle)
