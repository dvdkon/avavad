import bottle

def register_cors_plugin(app):
    def cors_plugin(callback):
        def wrapper(*args, **kwargs):
            app.response.headers["Access-Control-Allow-Origin"] = "*"
            app.response.headers["Access-Control-Allow-Methods"] = \
                "GET, POST, OPTIONS"
            app.response.headers["Access-Control-Allow-Headers"] = \
                "Origin, Accept, Content-Type, X-Requested-With, X-Avavad-Auth"
            if app.request.method != "OPTIONS":
                return callback(*args, **kwargs)
            return ""

        return wrapper

    app.install(cors_plugin)

    route_inner = app.route
    def route_wrapper(*args, **kwargs):
        methods = kwargs.get("method", ["GET"])
        if(isinstance(methods, str)):
            methods = {methods}
        else:
            methods = set(methods)
        methods.add("OPTIONS")
        kwargs["method"] = list(methods)
        return route_inner(*args, **kwargs)
    app.route = route_wrapper
