import functools
import hashlib
import hmac
import datetime as dt
from bottle import route, request, response
from avavad.tables import *
from avavad import config

def auth_token_for_pwauth(pwauth):
    text = f"pwauth@{pwauth.id}@{dt.datetime.now().isoformat()}"
    digest = hmac.digest(
        config.SECRET_KEY.encode(),
        text.encode(),
        "sha512").hex()
    return f"{text}!{digest}"

def verify_auth_token(token):
    token_s = token.split("!")
    if len(token_s) != 2: return False
    digest = hmac.digest(
        config.SECRET_KEY.encode(),
        token_s[0].encode(),
        "sha512").hex()
    return hmac.compare_digest(digest, token_s[1])

def get_current_auth_method(db):
    auth_token_s = request.headers["X-Avavad-Auth"].split("@")
    if auth_token_s[0] == "pwauth":
        return db.query(PasswordAuth) \
            .filter_by(id=int(auth_token_s[1])) \
            .first()
    else:
        raise Exception("Unimplemented")

def get_current_account(db):
    return get_current_auth_method(db).account

def auth_required(route_func):
    @functools.wraps(route_func)
    def new_func(*args, **kwargs):
        auth_token = request.headers.get("X-Avavad-Auth")
        if auth_token is None:
            response.status = 401
            return {"error": "No authentication token"}
        if not verify_auth_token(auth_token):
            response.status = 401
            return {"error": "Incorrect authentication token"}
        return route_func(*args, **kwargs)
    return new_func

def role_required(role):
    def decorator(route_func):
        @functools.wraps(route_func)
        @auth_required
        def new_func():
            account = get_current_account()
            correct_role = (isinstance(role, list) and account.role in role) \
                or account.role == role
            if not correct_role:
                return {"error": "Account does not have required role"}
            return route_func(*args, **kwargs)
        return new_func
    return decorator
