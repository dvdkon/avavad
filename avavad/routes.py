from bottle import route, request, response
from sqlalchemy import func
from avavad.tables import *
from avavad.auth import *

@route("/api/pw-auth/login", method="POST")
def login_password(db):
    pwauth = db.query(PasswordAuth) \
        .filter_by(username=request.forms.username) \
        .first()
    if pwauth is None:
        response.status = 401
        return {"error": "Incorrect username or password"}
    pwhash_split = pwauth.pwhash.split(":")
    pwhash = bytes.fromhex(pwhash_split[0])
    pwsalt = bytes.fromhex(pwhash_split[1])
    pw_given = request.forms.password
    pwhash_given = \
        hashlib.scrypt(pw_given.encode(), salt=pwsalt, **config.SCRYPT_PARAMS)
    if pwhash != pwhash_given:
        response.status = 401
        return {"error": "Incorrect username or password"}
    return {"token": auth_token_for_pwauth(pwauth)}

@route("/api/auth/current")
@auth_required
def current_auth(db):
    auth = get_current_auth_method(db)
    if isinstance(auth, PasswordAuth):
        return {"type": "pwuser", "id": auth.id, "username": auth.username}
    assert False

@route("/api/account/current")
@auth_required
def current_user(db):
    acc = get_current_account(db)
    return {
        "id": acc.id,
        "role": acc.role,
        "unix_user": None if acc.unix_user is None
                     else {"uid": acc.unix_user.uid,
                           "username": acc.unix_user.username},
        "pwauth": None if acc.password_auth is None
                  else {"username": acc.password_auth.username},
        "googleauth": None if acc.google_auth is None
                      else {"address": acc.google_auth.address},
    }

@route("/api/account/create", method="POST")
@role_required("admin")
def create_user(db):
    assert request.forms.role != ""
    db.add(Account(role=request.forms.role))

@route("/api/pw-auth/create", method="POST")
@auth_required
def create_pwauth(db):
    assert request.forms.username != "" \
        and request.forms.password != ""
    if db.query(PasswordAuth) \
       .filter(PasswordAuth.username == request.forms.username) \
       .count() > 0:
        response.status = 401
        return {"error": "Username already used"}
    if db.query(PasswordAuth) \
       .filter(PasswordAuth.account == get_current_account()) \
       .count() > 0:
        response.status = 401
        return {"error": "Account already has pwauth"}
    pa = PasswordAuth(
        username=request.forms.username,
        account=get_current_account(),
    )
    pa.set_password(request.forms.password)
    db.add(pa)

@route("/api/pw-auth/change", method="POST")
@auth_required
def change_pwauth(db):
    assert request.forms.password != ""
    auth = get_current_account(db).password_auth
    if auth is None:
        response.status = 400
        return {"error": "Can't change password of account without pwauth"}
    auth.set_password(request.forms.password)
    db.add(auth)

@route("/api/unix-user/create", method="POST")
@auth_required
def create_unix_user(db):
    assert request.forms.username != "" \
        and request.forms.password != ""
    account = get_current_account(db)
    # TODO: Fix race condition (it's not a data integrity issue, thankfully)
    # We also need to make sure not to reuse UIDs
    # Maybe a separate table? Or just never deleting users
    max_uid = db.query(func.max(UnixUser.uid)).scalar() \
        or min(config.UNIX_USER_UIDS) - 1
    unix_user = UnixUser(
        account=account,
        uid=max_uid + 1,
        username=request.forms.username,
    )
    unix_user.set_password(request.forms.password)
    db.add(unix_user)

@route("/api/unix-user/change", method="POST")
@auth_required
def change_unix_user(db):
    assert request.forms.username != "" \
        and request.forms.password != ""
    account = get_current_account(db)
    if account.unix_user is None:
        response.status = 400
        return {"error": "Account doesn't have UNIX user"}
    account.unix_user.username = request.forms.username
    account.unix_user.set_password(request.forms.password)
    db.add(account.unix_user)

@route("/api/unix-users")
@auth_required
def list_unix_users(db):
    return {"unix_users": [{
            "uid": u.uid,
            "username": u.username,
        } for u in db.query(UnixUser).all()]}
