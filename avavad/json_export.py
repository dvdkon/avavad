import pathlib
import json
from avavad.tables import *

def generate_json(db, outdir):
    unix_users = [{
            "uid": uu.uid,
            "username": uu.username,
            "pwhash": uu.pwhash,
            "role": uu.account.role,
        } for uu in db.query(UnixUser).all()]
    json.dump(unix_users, outdir / "users.json")
