#!/usr/bin/env python3
import bottle
from avavad import routes

if __name__ == "__main__":
    bottle.run(host="localhost", port=7020, debug=True)
