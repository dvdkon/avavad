#!/usr/bin/env python3
import sys
from sqlalchemy import create_engine
from avavad import tables

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print(f"Usage: {sys.argv[0]} <db connstr>", file=sys.stderr)
        sys.exit(1)

    engine = create_engine(sys.argv[1])
    tables.Base.metadata.create_all(engine)
