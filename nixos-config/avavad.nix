{ ... }:

let dataDir = /etc/nixos/avavad-data;
in (import ./users.nix) { dataDir = dataDir; }
