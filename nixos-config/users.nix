{ dataDir }:

with builtins;
{
    users.users =
        let
            usersData = fromJSON (readFile (dataDir + "/users.json"));
        in listToAttrs (map (u: {
            name = u.username;
            value = {
                isNormalUser = true;
                uid = u.uid;
                hashedPassword = u.pwhash;
                extraGroups = if u.role == "admin" then ["wheel"] else [];
            };
        }) usersData);
}
