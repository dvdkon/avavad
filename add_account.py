#!/usr/bin/env python3
import sys
import os
import hashlib
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from avavad.tables import *
from avavad import config

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print(f"Usage: {sys.argv[0]} <role> <username>", file=sys.stderr)
        sys.exit(1)

    engine = create_engine(config.DB_CONNSTR)
    Session = sessionmaker(bind=engine)
    db = Session()

    pw = input("Password: ")
    salt = os.urandom(32)
    pwhash = hashlib.scrypt(pw.encode(), salt=salt, **config.SCRYPT_PARAMS)
    pwhash_db = f"{pwhash.hex()}:{salt.hex()}"

    acc = Account(role=sys.argv[1])
    pwauth = PasswordAuth(username=sys.argv[2], pwhash=pwhash_db, account=acc)
    db.add(acc)
    db.add(pwauth)
    db.commit()
